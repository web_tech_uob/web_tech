import { callApi } from '../services'

export default {
   add: (post) => callApi('http://localhost:8000/forum/add', {
    method: 'POST',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(post),
  }),
  get: (query) => callApi(`http://localhost:8000/forum`, {
    method: 'GET',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
  }),
}
