function checkHttpStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

function parseJSON (response) {
  return response.json()
}

export const callApi = (url, options) => {
  return fetch(url, options)
    .then(checkHttpStatus)
    .then(parseJSON)
}

export { default as Member } from './member';
export { default as Product } from './product';
export { default as Post } from './post';
