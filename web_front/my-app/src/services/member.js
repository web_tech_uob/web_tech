import { callApi } from '../services'

export default {
  fetch: (query) => callApi(`http://localhost:8000/users/${query}`, {
    method: 'GET',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
    // body: JSON.stringify(query)
  }),
  create: (member) => callApi('http://localhost:8000/users/create', {
    method: 'POST',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(member)
  }),
  update: (member) => callApi(`http://localhost:8000/users/${member.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(member)
  }),
  verify: (member) => callApi('http://localhost:8000/users/username', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(member)
  }),

}
