import { callApi } from '../services'

export default {
   create: (product) => callApi('http://localhost:8000/products/create', {
    method: 'POST',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(product),
  }),
  fetch: (query) => callApi(`http://localhost:8000/products`, {
    method: 'GET',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
  }),
  category:(query)=>callApi(`http://localhost:8000/categories`, {
    method: 'Post',
    headers: {
      'X-Powered-By': 'Express',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(query),
  })
}