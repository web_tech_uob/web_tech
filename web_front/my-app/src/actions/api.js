import { createAction } from 'redux-actions'
import * as types from '../constants'

// Actions Called by Backend
export const members = {
  create: {
    request: createAction(types.CREATE_MEMBER_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.CREATE_MEMBER_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.CREATE_MEMBER_FAILURE, (meta, error) => error, meta => meta)
  },
  update: {
    request: createAction(types.UPDATE_MEMBER_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.UPDATE_MEMBER_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.UPDATE_MEMBER_FAILURE, (meta, error) => error, meta => meta)
  },
  fetch: {
    request: createAction(types.FETCH_MEMBER_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_MEMBER_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_MEMBER_FAILURE, (meta, error) => error, meta => meta)
  },
  verify: {
    request: createAction(types.VERIFY_MEMBER_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.VERIFY_MEMBER_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.VERIFY_MEMBER_FAILURE, (meta, error) => error, meta => meta)
  }
}
export const products = {
  create: {
    request: createAction(types.CREATE_PRODUCT_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.CREATE_PRODUCT_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.CREATE_MEMBER_FAILURE, (meta, error) => error, meta => meta)
  },
  fetch: {
    request: createAction(types.FETCH_PRODUCT_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_PRODUCT_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_PRODUCT_FAILURE, (meta, error) => error, meta => meta)
  },
  category: {
    request: createAction(types.FETCH_CATEGORY_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_CATEGORY_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_CATEGORY_FAILURE, (meta, error) => error, meta => meta)
  }
}

export const post = {
  add: {
    request: createAction(types.ADD_MESSAGE_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.ADD_MESSAGE_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.ADD_MESSAGE_FAILURE, (meta, error) => error, meta => meta)
  },
  get: {
    request: createAction(types.GET_MESSAGE_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.GET_MESSAGE_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.GET_MESSAGE_FAILURE, (meta, error) => error, meta => meta)
  },
}
