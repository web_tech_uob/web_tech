import { createAction } from 'redux-actions'
import * as types from '../constants'

// Frontend Action
export const fetchMember = createAction(types.FETCH_MEMBER, query => query)
export const verifyMember = createAction(types.VERIFY_MEMBER, member => member)
export const createMember = createAction(types.CREATE_MEMBER, member => member)
export const updateMember = createAction(types.UPDATE_MEMBER, member => member)
export const deleteMember = createAction(types.DELETE_MEMBER, id => id)

export const createProduct = createAction(types.CREATE_PRODUCT, product=> product)
export const fetchProduct = createAction(types.FETCH_PRODUCT, query => query)
export const fetchCategory = createAction(types.FETCH_CATEGORY, query => query)

export const addMessage = createAction(types.ADD_MESSAGE, post => post)
export const getMessage = createAction(types.GET_MESSAGE, query => query)
