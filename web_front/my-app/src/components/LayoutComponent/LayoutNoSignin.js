import React from "react";
import './Layout.css'

import { Navbar } from 'react-bootstrap';
import { SigninPage } from '../SigninPage'

const LayoutNoSignin =(props)=>{

  return(
   <div>
    <Navbar  expands="lg" id = "navbar-brand" bg="white" variant="light" sticky="top">
    <Navbar.Brand className="align-bottom" href="/registration">
      <img
        alt=""
        src="/images/bristol-logo.jpg"
        width="100"
        height="70"
      />
      {'Welcome to the Bristol Social Network'}
    </Navbar.Brand>
    <SigninPage verifyMember ={props.verifyMember} message = {props.message}/>
</Navbar>

<div id = "bg">
</div>
</div>
)}

export default LayoutNoSignin;
