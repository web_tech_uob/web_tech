import React from 'react'
import { Form, FormControl, Button, Navbar, Nav, NavDropdown, Image, Col } from 'react-bootstrap'
import {  useCookies } from 'react-cookie';

import './Layout.css';
const Layout = (props) => {
  const [cookies, setCookie, removeCookie] = useCookies(['login']);

  const logout = () =>{
    removeCookie('login',{ path:'/'})
    window.location.replace('/registration')
  }
  return (
   <div className="h-100"> 
     <Navbar className='nav-text' collapseOnSelect expand="lg" bg="dark" variant="dark">
       <Navbar.Brand href="/">Bristol Social Network</Navbar.Brand>
       <Navbar.Toggle aria-controls="basic-navbar-nav" />
       <Navbar.Collapse id="basic-navbar-nav">
         <Nav className="mr-auto">
            <Nav.Link href="/secondhand">Second-Hand Board</Nav.Link>
          </Nav>
          <Nav className="mr-auto">
          <Form inline>
              <FormControl type="text" placeholder="Search" className="mr-sm-1" />
              <Button variant="outline-secondary">
              <Image className="img" src={process.env.PUBLIC_URL +'/images/material/search.svg'}></Image>
              </Button>
            </Form>
            <NavDropdown title="Settings" id="basic-nav-dropdown">
              <NavDropdown.Item href="/setting/profile">User Profile</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Message</NavDropdown.Item>
              <NavDropdown.Divider />
              <div className="d-flex justify-content-center" >
              <Button variant="light" size='sm' onClick={logout}>Logout</Button>
              </div>
            </NavDropdown>
         </Nav>
       </Navbar.Collapse>
     </Navbar>
     {props.children}
     </div>
     
);}

export default Layout;