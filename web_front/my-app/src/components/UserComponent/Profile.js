import React , { useState } from 'react'
import { Container, Col, Row } from 'react-bootstrap'
import { Layout } from '../LayoutComponent';
import { UserInform, UserPhoto } from '../UserComponent'
import { useCookies } from 'react-cookie';

const Profile = (props) => {

    // Initialise the state
 
  const [userId, setUserId] = useState(props.match.params.id);
  const [cookies, setCookie] = useCookies(['login']);
  if (props.failure){
    console.log(props.failure)
    window.location.replace('/404')
  }
  if(userId===undefined){
    setUserId(cookies.login)
  }

  return(
    <Layout>
      <div id="bg2" className="p-4">

      
      <Container >
          <UserPhoto 
            fetchMember = {props.fetchMember}
            updateMember = {props.updateMember}
            id = {userId}
            profiles = {props.profile}
          />
          <UserInform 
            fetchMember = {props.fetchMember}
            id = {userId}
            profiles = {props.profile}
          />
      </Container>
      </div>
    </Layout>
  )
};

export default Profile
