import React , { useState,useEffect } from 'react'
import { Image, Col, Container, Row } from 'react-bootstrap'
import './user.css'
const UserPhoto = (props) =>{

    // Initialise the state
   const [url, setUrl] = useState('')
   const [id, setId] = useState(props.id)
   const [update, setUpdate] = useState(false)
   const loadphoto=()=>{
      props.fetchMember(id);
   }
   useEffect(()=>{
      if (update===false){
         loadphoto();
         setUpdate(true);
      }
      try{
        setUrl(props.profiles.photo);
      }catch{
        console.log("Error")
      }
    },[props.profiles.photo])   
   
  
   return(
    <div className=" d-flex justify-content-center">
          <Image className="userphoto mt-2" src={`http://localhost:8000/${url}`}></Image>
    </div>
  )
};

export default UserPhoto