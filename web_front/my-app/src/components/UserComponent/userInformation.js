import React , { useState, useEffect } from 'react'
import { Card } from 'react-bootstrap'
import './user.css'

const UserInform = (props) =>{

    // Initialise the state
   const [userId, setUserId] = useState(props.id);
   const [userName, setUserName] = useState('')
   const [foreName, setForeName] = useState('')
   const [surName, setSurName] = useState('')
   const [birth, setBirth] = useState('')
   const [status, setStatus] = useState('')
   const [update, setUpdate] = useState(false)


   const loadmember=()=>{
      if(userId!==undefined){
         props.fetchMember(userId);
      }
   }
  // Load member data
   useEffect(()=>{
    if (update===false){
      loadmember();
      setUpdate(true);
    }
    try{
      setUserName(props.profiles.userName);
      setForeName(props.profiles.foreName);
      setSurName(props.profiles.surName);
      setBirth(props.profiles.birth);
      if(props.profiles.status){
        setStatus("Studying");
      }else{
        setStatus("Graduated");
      }
    }catch{
      console.log("Error")
    }
   },[props.profiles.userName,props.profiles.foreName,props.profiles.surName,props.profiles.birth,props.profiles.email])
  
  return(
    <div className="userInfo mt-4">
      <Card className=" justify-content-center mt-2 " variant="flush" >
        <Card.Title className="text-center">{foreName} {surName} Info</Card.Title>
        <Card.Text>Email: {userName}</Card.Text>
        <Card.Text>Birth: {birth} </Card.Text>
        <Card.Text>Status: {status} </Card.Text>
      </Card>
    </div>
  )
};

export default UserInform