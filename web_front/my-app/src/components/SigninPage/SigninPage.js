import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import './signin.css'
const SigninPage = (props) => {
   const [ userName, setUsername ] = useState('');
   const [ password, setPassword ] = useState('');
   const[messages, setMessage]=useState(props.message)

   const handleSubmit = () => {
      const formValues = {
         userName,
         password
      }
      props.verifyMember(formValues)
      
   }
   useEffect(()=>{
      setMessage(props.message)
    },[props.message])
   return (
      <Form className="w-100 align-bottom">
         <Row className="mt-4">
            <Col md={4}>
               <Form.Group  controlId="userName" >
                  <Row>
                     <Form.Label column sm="3">userName</Form.Label>
                     <Col >
                     <Form.Control type="email" placeholder="Enter userName" onChange={e => setUsername(e.target.value)} />
                     </Col>
                  </Row>
               </Form.Group>           
               </Col>
               <Col md={4}>
                  <Form.Group controlId="password">
                  <Row>
                     <Form.Label column sm="3">password</Form.Label>
                     <Col>
                        <Form.Control type="password" placeholder="Enter password" onChange={e => setPassword(e.target.value)} />
                     </Col>
                  </Row>
                  </Form.Group> 
               </Col>
               <Col md={3}>
                  <Row>
                     <Button size="sm" variant="primary" type="button" onClick={handleSubmit}>Login</Button>
                  </Row>
                  <Row>
                     <Form.Text className="warning">{messages}</Form.Text>
                  </Row>
               </Col>
            </Row>
         </Form>
      )
   }

export default SigninPage;
