import React from 'react'
import { LayoutNoSignin } from '../LayoutComponent'
import { Container, Jumbotron } from 'react-bootstrap';
import './errorpage.css'
const Errorpage = (match) => {
   console.log(match)
   return (
      <div>
         <LayoutNoSignin/>
         <Container className="mt-4">
            <Jumbotron>
            <h1 className="errorTitle">This page isn't available </h1>
            <h3 className="pt-5">The link you followed may be broken, or you try to be a Bristolian hacker</h3>
            <h2 className="pt-5 linkText"><a className="text-primary text-decoration-none" href="/">Go back the home page</a></h2>
            </Jumbotron>
         </Container>
      </div>
   )
}

export default Errorpage