import React, { useState } from 'react'
import { Form,Button,Nav,Image, Modal, Row, Col, Card,NavDropdown, Navbar } from 'react-bootstrap';
import './SecondHand.css'


function zip(a, b, c, d, e,f) {
   var arr = [];
   for (var key in a) arr.push([key,a[key], b[key], c[key], d[key], e[key],f[key]]);
   return arr;
}

const SecondHandSearchBar= (props) =>{
   const [display, setDisplay] = useState(props.categoryDisplay);
   const [itemExist, setItemExist] = useState(false);
   const [defaultShow, setDefault] = useState(false);
   const [previewName, setPreview] = useState([""]);
   const [message, setMessage] = useState(props.messages);

   const loadproduct=(id)=>{
      const query = {
         id
      }
      props.fetchCategory(query);
      setDisplay(true)
   }
   const searchItem = ( text ) =>{
      var arr = [];
      for (var i in props.boards.productNames){
         arr.push(props.boards.productNames[i].toLowerCase())
      }
      var indexArr = []
      const element = arr.filter(function(item,index){
         var regex = new RegExp('.*'+text+'.*','gm')
         if(regex.test(item)){
            indexArr.push(index)
         }
         regex.compile('.*'+text+'.*','gm')
         return regex.exec(item);
      })
      if(indexArr.length!==0){
         setItemExist(true);
         const itemNames = props.boards.productNames.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         if(text!=="")
         {
            setPreview(itemNames);
            setDefault(true);
         }else{
            setDefault(false);
         }
         const itemDescriptions= props.boards.descriptions.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         const itemUsernaems = props.boards.userNames.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         const itemphotopaths = props.boards.photopaths.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         const itemphotos = props.boards.photos.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         const itemuserIds = props.boards.userIds.filter(function(item,index){
            if(indexArr.includes(index)){
               return item;
            }
            return null;
         })
         props.categories.productNames = itemNames;
         props.categories.descriptions = itemDescriptions;
         props.categories.photo = itemphotos ;
         props.categories.userIds = itemuserIds;
         props.categories.photopaths = itemphotopaths;
         props.categories.userNames = itemUsernaems;
         setMessage("")

      }else{
         props.categories.productNames = "";
         props.categories.descriptions = "";
         props.categories.photo = "" ;
         props.categories.userIds = "";
         props.categories.photopaths = "";
         props.categories.userNames = "";
         setMessage("Not found any related product")
      }
   }
   const showItem = ()=>{
      if(itemExist===true){
         setDisplay(true);
      }else{
         setDisplay(true);
      }
   }
   const selectPreview = (key)=>{
      searchItem(props.categories.productNames[key].toLowerCase());
      showItem()
      setDefault(false);
   }
  return (
   <div className='sideBox rounded p-2' >
      <Nav className="flex-column">
         <Form inline>
            <Form.Control type="text" placeholder="Search your items" className="mr-sm-2" onChange={e=>{searchItem(e.target.value.toLowerCase());
}} />
            <Button size="sm" variant="outline-dark" onClick={showItem}>
            <Image className="img" src={process.env.PUBLIC_URL +'/images/material/search.svg'}></Image>
            </Button>    
         </Form>
         <NavDropdown   show={defaultShow} onSelect={selectPreview} >
            <NavDropdown.Item >{message}</NavDropdown.Item>
            <Nav className="productPreview">
               { previewName.map((name,index)=>
                  <NavDropdown.Item eventKey={index}> {name}
               </NavDropdown.Item>)}
            </Nav>
            
         </NavDropdown>
         <Nav.Item className="boxName mt-2">
            Categories
         </Nav.Item>
         <Nav.Link eventKey="1" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/home.svg'}/>
            Home
         </Nav.Link>
         <Nav.Link eventKey="2" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/clothes.svg'}/>
            Clothes
         </Nav.Link>
         <Nav.Link eventKey="3" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/shoes.svg'}/>
            Shoes
         </Nav.Link>
         <Nav.Link eventKey="4" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/book.svg'}/>
            Book
         </Nav.Link>
         <Nav.Link eventKey="5" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/tech.svg'}/>
            Technology
         </Nav.Link>
         <Nav.Link eventKey="6" onSelect={e=>{loadproduct(e);}}>
            <Image className="icon" src={process.env.PUBLIC_URL +'/images/handB.svg'}/>
            Health and Beauty
         </Nav.Link>
      </Nav>
      <Modal className="categorybox" show={display} onHide={()=>{setDisplay(false);}}>
      <div >      
         <Modal.Header className="title" closeButton>Category</Modal.Header>
         <div className="p-2 warning">{message}</div>
         <Row className="p-2" >
         { zip(props.categories.photopaths,
            props.categories.productNames,
            props.categories.descriptions, 
            props.categories.userNames,
            props.categories.userIds, props.categories.uploadTime).map(product=>
         <Col key={product[0]} className="product" id={`product_${product[2]}`} md={6}>
         <Card className="cardBox">
            <Card.Img variant="top"  src={`http://localhost:8000/${product[1]}`}></Card.Img>
            <Card.Body>
               <Card.Title>{product[2]}</Card.Title>
                <Card.Text>{product[3]} </Card.Text>
                <a href={`/profile/${product[5]}`}>{product[4]}</a>
                <Card.Text className="blockquote-footer">Upload at : {product[6]}</Card.Text>
              </Card.Body>
          </Card>
        </Col>
        )}
    </Row>
    </div>
      </Modal>
   </div>
  )
}

export default SecondHandSearchBar
