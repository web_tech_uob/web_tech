import React, { useState, useEffect } from 'react'
import { Row, Col, Card } from 'react-bootstrap'
// import ClipLoader from 'react-spinners/ClipLoader';

import './SecondHand.css'

function zip(a, b, c, d, e, f) {
  var arr = [];
  for (var key in a) arr.push([key,a[key], b[key], c[key], d[key], e[key],f[key]]);
  return arr.reverse();
}
const SecondHandBoard= (props) =>{
  const [productName, setproductName] = useState([""]);
  const [description, setDescription] = useState([""]);
  const [photo, setPhoto] = useState([]);
  const [uploadTime, setUploadTime] = useState([""]);
  const [userNames, setUserNames] = useState([""]);
  const [userIds, setUserIds] = useState([]);
  const [url, setURL] = useState([""]);
  const [update, setUpdate] = useState(false)

  const loadproduct=()=>{
    const query =
    {
      productName,
      description,
      photo,
      url
    }
    props.fetchProduct(query);
  }
  useEffect(()=>{
    if (update===false){
      loadproduct();
      setUpdate(true);
    }
    try{
      setproductName(props.boards.productNames);
      setDescription(props.boards.descriptions);
      setPhoto(props.boards.photos);
      setUserNames(props.boards.userNames);
      setUserIds(props.boards.userIds);
      setURL(props.boards.photopaths);
      setUserIds(props.boards.userIds);
      setUploadTime(props.boards.uploadTime);
    }catch{
      console.log("Error")
    }
  }, [props.boards.productNames, props.boards.descriptions, props.boards.photos, props.boards.photopaths, props.boards.userNames, props.boards.userIds,props.boards.uploadTime])    
  return (
    <Row className="p-2">
    <Row >
      { zip(url, productName, description, userNames, userIds, uploadTime).map(product=>
        <Col key={product[0]} className="product" id={`product_${product[2]}`} md={4}>
          <Card>
            <Card.Img variant="top"  src={`http://localhost:8000/${product[1]}`}></Card.Img>
              <Card.Body>
                <Card.Title>{product[2]}</Card.Title>
                <Card.Text>{product[3]} </Card.Text>
                <Card.Link href={`/profile/${product[5]}`}>{product[4]}</Card.Link> 
                <Card.Text className="blockquote-footer">Upload at : {product[6]} </Card.Text>
              </Card.Body>
          </Card>
        </Col>
        )};
    </Row>
    {/* <ClipLoader></ClipLoader> */}
    </Row>
)}


export default SecondHandBoard
