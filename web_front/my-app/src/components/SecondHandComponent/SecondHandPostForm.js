import React, { useState } from 'react'
import { Form, Button, Image, Modal } from 'react-bootstrap'
import './SecondHand.css'

const SecondHandPostForm= (props) =>{
  const [photo, setImage] = useState('')
  const [fileName, setFileName] = useState('')
  const [productName, setProductName] = useState('')
  const [description, setDescription] = useState('')  
  const [userId, setUserId] = useState(props.id);
  const [category, setCategory] = useState(0);
  const [validated, setValidated] = useState(false)
  const [preview, setPreview] = useState('');
  const [display, setDisplay] = useState(false);

  

  const handleSubmit = (e)=>{
    const form = e.currentTarget;
    if(form.checkValidity()){
      e.preventDefault();
      e.stopPropagation();
    }
    setValidated(true);
    const formValues = {
      userId,
      photo,
      fileName,
      productName,
      category,
      description
    }
    if(validated&&category!==0){
      props.createProduct(formValues)
      setDisplay(false);
      setPreview('');
    }
  }
  // useEffect(()=>{
  //   console.log(props.created)
  //   if(props.created===true){
  //     window.location.reload();
  //   }
  // })
  const getBase64 = (file) =>{
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  const showModal = ()=>{
    setDisplay(true);
  }
  return (
    <div className="rounded p-2">
    <Button variant="light" className="box_title font-weight-bold mt-4 rounded" onClick={showModal}>
      Share anything you want to sell
    </Button>
    <Modal className="SideBox " show={display} onHide={()=>{setDisplay(false);}}>
      <Modal.Header className="font-weight-bold" closeButton >What do you want to sell today</Modal.Header>
    
    <Form noValidate validated={validated}>
      <Form.Group>
         <Form.Label>Product Name</Form.Label>
         <Form.Control type="text" placeholder="product" onChange={e => setProductName(e.target.value)} required/>
         <Form.Control.Feedback type="invalid">Please Provide Product Name </Form.Control.Feedback>
      </Form.Group>
      <Form.Group>
         <Form.Label>Product Description</Form.Label>
         <Form.Control type="text" placeholder="description" onChange={e => setDescription(e.target.value)} required/>
         <Form.Control.Feedback type="invalid">Please Provide Description </Form.Control.Feedback>
      </Form.Group>
      <Form.Group>
         <Form.Label>Product Photo</Form.Label>
         <Form.Control type="file" name="photo" placeholder="Image"  multiple onChange={e =>{
            setPreview(URL.createObjectURL(e.target.files[0]))
            getBase64(e.target.files[0]).then(
              data => setImage(data)
            ); 
            setFileName(e.target.files[0].name);
          }
        }/>
      </Form.Group>
      <Image className = "preview" src={preview}></Image>

      <Form.Group>
         <Form.Label>Product Category</Form.Label>
         <Form.Control as="select" onChange={e => setCategory(e.target.value)}>
            <option >Please Choose the Category</option>
            <option value="1" >Home</option>
            <option value="2" >Clothes</option>
            <option value="3" >Shoes</option>
            <option value="4" >Books</option>
            <option value="5" >Technology</option>
            <option value="6" >Health and Beauty</option>
          </Form.Control>
      </Form.Group>
      <Button variant="outline-secondary" size="sm" onClick={e=> handleSubmit(e)}>
      <Image className="img" src={process.env.PUBLIC_URL +'/images/material/upload.svg'}></Image>
      </Button>  

    </Form> 
    </Modal>
    </div>
  )
}



export default SecondHandPostForm