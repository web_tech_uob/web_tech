import React, { useState } from 'react'
import { Col, Row, Container } from 'react-bootstrap'
import './SecondHand.css'
import { SecondHandBoard, SecondHandPostForm, SecondHandSearchBar } from '.';
import { Layout } from '../LayoutComponent';
import { useCookies } from 'react-cookie';

const SecondHand= (props) =>{

  const [userId, setUserId] = useState(props.match.params.id);
  const [cookies, setCookie] = useCookies(['login']);
  if(userId===undefined){
    console.log(cookies.login)
    setUserId(cookies.login)
  }
  return (
    <Layout>
    <div className='mt-4'>
      <Row>
      <Col sm={2} >
        <SecondHandSearchBar
          boards = {props.board}
          fetchCategory = {props.fetchCategory}
          categories = {props.categories}
          categoryDisplay = {props.display}
        />
      </Col>
      <Col sm={6} >
        <Row>
          <SecondHandPostForm
          id = {userId}
          createProduct = {props.createProduct}
          created = {props.productCreated}
          />
        </Row>
        <Row className='mt-2'>
        <SecondHandBoard
        fetchProduct = {props.fetchProduct}
        boards = {props.board}
        messages = {props.message}
        />
        </Row>
      </Col>
      </Row>

    </div>
    </Layout>
  )
}



export default SecondHand
