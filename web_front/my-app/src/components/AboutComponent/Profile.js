import React from 'react';
import './Profile.css'
import { Carousel, Card, Container, Col, Row, ListGroup } from 'react-bootstrap'

const Profile = () =>{ return (
  <Carousel >
    <Carousel.Item >
      <div className="d-flex justify-content-center list">
      <ListGroup variant="flush" className="mr-2 list_box">
        <ListGroup.Item className="list">
            Ting-Hung Chen
        </ListGroup.Item>
        <ListGroup.Item>
        University of Bristol
        </ListGroup.Item>
        <ListGroup.Item>
        Backend Design
        </ListGroup.Item>
        <ListGroup.Item>
          Big MaMa
        </ListGroup.Item>
        <ListGroup.Item>
          Eating Monster
        </ListGroup.Item>
      </ListGroup>
      <img
        className="d-block profileImg"
        src={process.env.PUBLIC_URL +'/images/david.jpeg'}
        alt="First slide"
      />
      </div>
    </Carousel.Item>
  <Carousel.Item >
    <div className="d-flex justify-content-center list">
    <ListGroup variant="flush" className="mr-2 list_box">
      <ListGroup.Item className="list">
        Yu-Ting Lan
      </ListGroup.Item>
      <ListGroup.Item>
      University of Bristol
      </ListGroup.Item>
      <ListGroup.Item>
      Front-end Design and Deploy
      </ListGroup.Item>
      <ListGroup.Item>
      JavaScripts, Python, C, Java, Git, Django, React.js, Redux.js
      </ListGroup.Item>
      <ListGroup.Item>
        Taiwan
      </ListGroup.Item>
    </ListGroup>
    <img
      className="d-block profileImg"
      src={process.env.PUBLIC_URL +'/images/justin.jpg'}
      alt="Third slide"
    />
    </div>
  

  </Carousel.Item>
  <Carousel.Item >
  <div className="d-flex justify-content-center list">
  <ListGroup variant="flush" className="mr-2 list_box">
      <ListGroup.Item className="list">
        Otto Brookes
      </ListGroup.Item>
      <ListGroup.Item>
      University of Bristol
      </ListGroup.Item>
      <ListGroup.Item>
      Backend Design
      </ListGroup.Item>
    </ListGroup>
    <img
      className="d-block w-25"
      src={process.env.PUBLIC_URL +'/images/user.svg'}
      alt="Third slide"
    />
  </div>
  

  </Carousel.Item>
</Carousel>
);
}

export default Profile
