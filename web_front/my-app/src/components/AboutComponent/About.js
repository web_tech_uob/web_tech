import React from 'react'
// import {LayoutNoSignin} from '../LayoutComponent';
import Profile from './Profile'
import { Navbar, Nav } from 'react-bootstrap';

const About = () => (
  <div>
    <Navbar  expands="lg" id = "navbar-brand" bg="white" variant="light" sticky="top">
      <Navbar.Brand className="align-bottom" href="/registration">
        <img
          alt=""
          src="/images/bristol-logo.jpg"
          width="100"
          height="70"
        />
        {'Welcome to the Bristol Social Network'}
      </Navbar.Brand>
      <Nav variant="light" >
        <Nav.Link  href="/registration">Home</Nav.Link>
        <Nav.Link  href="/signup">SignUp</Nav.Link>
      </Nav>
    </Navbar>
    <div id="bg"></div>
    <Profile></Profile>
  </div>

);

export default About
