import React, { useState } from 'react'
import { Form, Button, Image, Col, Row, Dropdown, Modal } from 'react-bootstrap'
import './Forum.css'

const ForumPost = (props) => {
  const [message, setMessage] = useState('');
  const [title, addTitle] = useState('');
  const [userId, setUserId] = useState(props.id);
  const [validated, setValidated] = useState(false);
  const [photo, setImage] = useState('')
  const [preview, setPreview] = useState('');
  const [fileName, setFileName] = useState('')
  const [display, setDisplay] = useState(false);
  const handleSubmit = (e) => {
    const form = e.currentTarget;
    if(form.checkValidity()){
      e.preventDefault();
      e.stopPropagation();
    }
    setValidated(true);
    const formValues = {
      title,
      photo,
      fileName,
      message,
      userId
    }
    if(validated){
      props.addMessage(formValues)
      setDisplay(false);
      setPreview('');

    }
  }
  const getBase64 = (file) =>{
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  const showModal = ()=>{
    setDisplay(true);
  }
  return (
    <div className="forum_post">
      <Button variant="light" className="box_title font-weight-bold mt-4 rounded" onClick={showModal}>Share the new information with your Bristolian</Button>
    <Modal className="forumSideBox " show={display} onHide={()=>{setDisplay(false);}} >
      <Modal.Header className="font-weight-bold" closeButton >What do you want to share today</Modal.Header>
    <Form className="p-3 forumForm" noValidate validated={validated}>
      <Form.Group>
         <Form.Label>Post title</Form.Label>
         <Form.Control type="text" placeholder="Title" onChange={e => addTitle(e.target.value)} required/>
         <Form.Control.Feedback type="invalid">Please provide a title </Form.Control.Feedback>
      </Form.Group>
      <Form.Group>
         <Form.Label>Post body</Form.Label>
         <Form.Control as="textarea" rows="5" placeholder="Give me some interesting news......" onChange={e => setMessage(e.target.value)} required/>
         <Form.Control.Feedback type="invalid">Please provide a post </Form.Control.Feedback>
      </Form.Group>
      <Row>

      <Col>
      <Form.Group>
         <Form.Label>Product Photo</Form.Label>
         <Form.Control type="file" name="photo" placeholder="Image"  multiple onChange={e =>{
           setPreview(URL.createObjectURL(e.target.files[0]))
            getBase64(e.target.files[0]).then(
              data => setImage(data)
            ); 
            setFileName(e.target.files[0].name);
          }
        }/>
      </Form.Group>
      </Col>
      <Col>
      <Button variant="outline-secondary" size="sm" onClick={e=> handleSubmit(e)} >
      <Image className="img" src={process.env.PUBLIC_URL +'/images/material/upload.svg'}></Image>
      </Button>
      </Col>
      </Row>
      <Image className = "preview" src={preview}></Image>
      
    </Form>
    </Modal>
    </div>
  )
}

export default ForumPost;
