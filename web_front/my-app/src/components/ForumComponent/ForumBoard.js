import React, { useState, useEffect } from 'react'
import { Col, Row, Container, Card } from 'react-bootstrap'
import './Forum.css'
import { Layout } from '../LayoutComponent';

function zip(a, b, c, d, e, f, g) {
  var arr = [];
  for (var key in a) arr.push([key,a[key], b[key], c[key], d[key],e[key],f[key],g[key]]);
  return arr.reverse();
}

const ForumBoard = (props) =>{
    const [userIds, setUserIds] = useState([]);
    const [message, setMessage] = useState([""]);
    const [title, addTitle] = useState([""]);
    const [userNames, setUserNames] = useState([""])
    const [postTime, setPostTime] = useState([""])
    const [update, setUpdate] = useState(false)
    const [photo, setPhoto] = useState([]);
    const [url, setURL] = useState([""]);

    const loadMessage =()=>{
      const query =
      {
        title,
        message,
        userNames,
      }
      props.getMessage(query);
    }
    useEffect(()=>{
      if (update===false){
        loadMessage();
        setUpdate(true);
      }
      try{
        addTitle(props.postBoard.titles);
        setMessage(props.postBoard.messages);
        setUserNames(props.postBoard.userNames);
        setPostTime(props.postBoard.postTimes);
        setUserIds(props.postBoard.userIds);
        setURL(props.postBoard.photopaths);
        setPhoto(props.postBoard.photos);

      }catch{
        console.log("Error")
      }
      
  }, [props.postBoard.titles, props.postBoard.photos, props.postBoard.photopaths ,props.postBoard.messages, props.postBoard.userNames, props.postBoard.postTimes])
  return (
    <div className="p-2">
      { zip(message, title, userNames, userIds, postTime, photo, url).map(post =>
          <Card key={post[0]} className="forum_board mt-2">
              <Card.Body>
                <Card.Text ><a href={`/profile/${post[4]}`}>{post[3]}</a> {post[5]}</Card.Text>
                <Card.Title>{post[2]}</Card.Title>
                <Card.Text className="d-flex justify-content-left">{post[1]} </Card.Text>
                <Card.Img className="d-flex justify-content-center"  src={`http://localhost:8000/${post[7]}`}></Card.Img>
                
                
              </Card.Body>
          </Card>
        )}
    </div>
)}

export default ForumBoard
