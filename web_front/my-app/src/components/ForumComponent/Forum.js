import React, { useState } from 'react'
import { Col, Row, Container, Card } from 'react-bootstrap'
import './Forum.css'
import { ForumPost } from '.';
import { ForumBoard } from '.';
import { Layout } from '../LayoutComponent';
import { useCookies } from 'react-cookie';

const Forum = (props) =>{
  const [userId, setUserId] = useState(props.match.params.id);
  const [cookies, setCookie] = useCookies(['login']);
  if(userId===undefined){
    console.log(cookies.login)
    setUserId(cookies.login)
  }

  return (
    <Layout>
    <Container>
        <ForumPost
        id = {userId}
        addMessage = {props.addMessage}
        />
        <ForumBoard
        getMessage = {props.getMessage}
        postBoard = {props.postBoard}
        />
    </Container>
    </Layout>
  )
}

export default Forum
