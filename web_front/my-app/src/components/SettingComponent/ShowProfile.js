import React , { useState, useEffect } from 'react'
import { Col, Row, Container } from 'react-bootstrap'
import { Layout } from '../LayoutComponent';
import { DialogueBox, UserInform, UserDetails } from '../UserComponent'
import UserPhoto from './UserPhoto';
import DialogueDetailBox from './dialogueBoxDetail';
import { useCookies } from 'react-cookie';

const Profile = (props) => {

    // Initialise the state
  const [foreName, setForeName] = useState('')
  const [userName, setUserName] = useState('')
  const [surName, setSurName] = useState('')
  const [birth, setBirth] = useState('')
  const [email, setEmail] = useState('')
  const [major, setMajor] = useState('')
  const [update, setUpdate] = useState(false)
  const [userId, setUserId] = useState(props.match.params.id);
  const [cookies, setCookie] = useCookies(['login']);

  if(userId==undefined){
    setUserId(cookies.login)
  }

  return(
    <Layout>
      <Container>
        <UserPhoto 
          fetchMember = {props.fetchMember}
          updateMember = {props.updateMember}
          id = {userId}
          profiles = {props.profile}
        />
        <UserInform 
          fetchMember = {props.fetchMember}
          id = {userId}
          profiles = {props.profile}
        />
      </Container>
    </Layout>
  )
};

export default Profile
