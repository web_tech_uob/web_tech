import React , { useState } from 'react'
import {  Button, Image, Modal, Form } from 'react-bootstrap'


const DialogueBox = (props) =>{

    // Initialise the state
  const [Display, setDisplay] = useState(false);
  const [id, setId] = useState(props.id)
  const [foreName, setForeName] = useState(props.fname)
  const [surName, setSurName] = useState(props.sname)
  const [birth, setBirth] = useState(props.birth)
  const [status, setStatus]= useState('')

  // Handle submit and send the action
  const handleSubmit = () => {
    const formValues = {
      id,
      foreName,
      surName,
      birth,
      status,
    
    }
    props.updateMember(formValues)
  }

  return(
  <div>
    <Button onClick={()=>{setDisplay(true);}}  variant="light" className="mt-2 float-right"><Image src={process.env.PUBLIC_URL +'/images/edit.svg'}></Image></Button>
    <Modal className="user_form" show={Display} onHide={()=>{setDisplay(false);}} >
        <Modal.Header closeButton>Change Your Profile</Modal.Header>
        <Form className="p-2">
        <Form.Group>
          <Form.Label>Surname</Form.Label>
          <Form.Control placeholder={props.sname} onChange={e => setSurName(e.target.value)} ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Forename</Form.Label>
          <Form.Control  placeholder={props.fname} onChange={e => setForeName(e.target.value)} ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Birth</Form.Label>
          <Form.Control type="date"  placeholder={props.birth} onChange={e => setBirth(e.target.value)}></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Status</Form.Label>
          <Form.Control as="select" onChange={e => setStatus(e.target.value)}>
            <option >Please Choose your status</option>
            <option value="1" >Studying</option>
            <option value="2">Graduated</option>
          </Form.Control>
        </Form.Group>
        <Modal.Footer>
          <Button onClick={()=>setDisplay(false)} className="float-right">Cancel</Button>
          <Button className="float-right" type="submit" onClick={handleSubmit}>Save</Button>
        </Modal.Footer>
        </Form>
      </Modal>
  </div>
  )
};

export default DialogueBox
