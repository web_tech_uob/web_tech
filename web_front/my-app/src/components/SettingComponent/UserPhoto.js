import React , { useState,useEffect } from 'react'
import { Form, Image, Button,Col, Container, Row } from 'react-bootstrap'
import './user.css'
const UserPhoto = (props) =>{

    // Initialise the state
   const [photo, setphoto] = useState('')
   const [url, setUrl] = useState('')
   const [fileName, setFileName] = useState('')
   const [id, setId] = useState(props.id)
   const [update, setUpdate] = useState(false)
   const [validated, setValidated] = useState(false)

   const loadphoto=()=>{
      props.fetchMember(id);
   }
   useEffect(()=>{
      if (update===false){
         loadphoto();
         setUpdate(true);
      }
      try{
        setUrl(props.profiles.photo);
      }catch{
        console.log("Error")
      }
    },[props.profiles.photo])   
   const getBase64 = (file) =>{
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });
    }
    const handleSubmit = (e) => {
      const form = e.currentTarget;
      if(form.checkValidity()){
        e.preventDefault();
        e.stopPropagation();
      }

      const formValues = {
        id,
        photo,
        fileName,
      }
      if(fileName!==""){
        setValidated(true);
        props.updateMember(formValues)
      }
    }
   return(
   <Container className=" mt-4">
      <div className="selftitle font-weight-bold">Selfile</div>      
      <Row className="d-flex justify-content-center">
          <Image className="userphoto mt-2 " src={`http://localhost:8000/${url}`}></Image>
      </Row>
      <Form className="d-flex justify-content-center" noValidate validated={validated} inline>
          <Form.Group className="mt-2" >
            <Form.Control type="file" name="photo" placeholder="Image"  multiple onChange={e =>{
                getBase64(e.target.files[0]).then(
                  data => setphoto(data)
                ); 
                setFileName(e.target.files[0].name);
              }
            } required/>
          </Form.Group>
          <Form.Control.Feedback type="invalid">Please Upload Selfie</Form.Control.Feedback>
          <Button variant="light" size="sm" onClick={e=> handleSubmit(e)}>
            <Image className="img" src={process.env.PUBLIC_URL +'/images/material/upload.svg'}></Image>
          </Button>  
      </Form>
    </Container>
  )
};

export default UserPhoto