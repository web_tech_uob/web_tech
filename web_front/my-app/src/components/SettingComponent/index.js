export { default as Profile } from './Profile';
export { default as SideNav } from './Sidenav';
export { default as DialogueBox } from './dialogueBox';
export { default as dialogueBoxDetail } from './dialogueBoxDetail';
export { default as UserDetails } from './userdetails';
export { default as UserInform } from './userInformation';
export { default as UserPhoto } from './UserPhoto';
