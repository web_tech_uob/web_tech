import React , { useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { Layout } from '../LayoutComponent';
import { DialogueBox, UserInform, UserDetails } from '../SettingComponent'
import UserPhoto from './UserPhoto';
import DialogueDetailBox from './dialogueBoxDetail';
import { useCookies } from 'react-cookie';

const Profile = (props) => {

    // Initialise the state
 
  const [userId, setUserId] = useState(props.match.params.id);
  const [cookies, setCookie] = useCookies(['login']);

  if(userId===undefined){
    setUserId(cookies.login)
  }

  return(
    <Layout>
      <div id="bg2" className="p-4">
     
      <Container>
        <Row>
          <Col md={5}>
            <UserPhoto 
            fetchMember = {props.fetchMember}
            updateMember = {props.updateMember}
            id = {userId}
            profiles = {props.profile}
            />
          </Col>
          <Col md={5}>
            <UserInform 
            fetchMember = {props.fetchMember}
            id = {userId}
            profiles = {props.profile}
            />
          <DialogueBox
          updateMember = {props.updateMember}
          id = {userId}
          uname = {props.profile.userName}
          fname = {props.profile.foreName}
          sname = {props.profile.surName}
          birth = {props.profile.birth}
          />
          </Col>
        </Row>
        <Row>
          <Col>
          <UserDetails
          fetchMember = {props.fetchMember}
          id = {userId}
          profiles = {props.profile}
        />
        <DialogueDetailBox
          updateMember = {props.updateMember}
          id = {userId}
          addrs = {props.profile.address}
          bio = {props.profile.biography}
          mjr = {props.profile.major}
        />
          </Col>
        
        </Row>
        </Container>
        </div>
    </Layout>
  )
};

export default Profile
