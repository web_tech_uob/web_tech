import React , { useState } from 'react'
import {  Button, Image, Modal, Form } from 'react-bootstrap'


const DialogueDetailBox = (props) =>{

    // Initialise the state
  const [Display, setDisplay] = useState(false);
  const [id, setId] = useState(props.id)
  const [address, setAddress] = useState(props.addrs)
  const [biography, setBiography] = useState(props.bio)
  const [major, setMajor] = useState(props.mjr)

  // Handle submit and send the action
  const handleSubmit = () => {
    const formValues = {
      id,
      address,
      biography,
      major
    }
    props.updateMember(formValues)
  }

  return(
  <div>
    <Button onClick={()=>{setDisplay(true);}}  variant="light" className="mt-2 float-right"><Image src={process.env.PUBLIC_URL +'/images/edit.svg'}></Image></Button>
    <Modal className="user_form" show={Display} onHide={()=>{setDisplay(false);}} >
        <Modal.Header closeButton>Change Your Information</Modal.Header>
        <Form className="p-2">
        <Form.Group>
          <Form.Label>Address</Form.Label>
          <Form.Control placeholder={props.addrs} onChange={e => setAddress(e.target.value)} ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Major</Form.Label>
          <Form.Control as="select"  placeholder={props.mjr} onChange={e => setMajor(e.target.value)} >
            <option >Please Choose your Major</option>
            <option value="Compuer Science" >Compuer Science</option>
            <option value="Electrical and Electronic Engineering">Electrical and Electronic Engineering</option>
            <option value="Engineering Mathematic">Engineering Mathematic </option>
            <option value="Aerospace Engineering">Aerospace Engineering</option>
            <option value="Civil Engineering">Civil Engineering</option>
            <option value="Mechanical Engineering">Mechanical Engineering</option>
            </Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Biography</Form.Label>
          <Form.Control as="textarea" onChange={e => setBiography(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Modal.Footer>
          <Button onClick={()=>setDisplay(false)} className="float-right">Cancel</Button>
          <Button className="float-right" type="submit" onClick={handleSubmit}>Save</Button>
        </Modal.Footer>
        </Form>
      </Modal>
  </div>
  )
};

export default DialogueDetailBox