import React , { useState,useEffect } from 'react'
import { Card } from 'react-bootstrap'
import './user.css'

const UserDetails = (props) =>{

    // Initialise the state
   const [biography, setBio] = useState('')
   const [major, setMajor] = useState('')
   const [address, setAddress] = useState('')
   const [update, setUpdate] = useState(false)

   const loadmember=()=>{
      props.fetchMember(props.id);
   }
  // Load member data
   useEffect(()=>{
   if (update===false){
      loadmember();
      setUpdate(true);
   }
   try{
      setBio(props.profiles.biography);
      setMajor(props.profiles.major);
      setAddress(props.profiles.address);

   }catch{
      console.log("Error")
   }
   },[props.profiles.biography,props.profiles.major,props.profiles.address])
   return(
   <div className=" userInfo mt-4">
   <Card className=" mt-2 " >
      <Card.Title className="text-center">Other Information</Card.Title>
        <Card.Text>Address: {address}
        </Card.Text>
        <Card.Text>Major:{major} </Card.Text>
        <Card.Text>Biography: {biography} </Card.Text>
    </Card>
    </div>
  )
};

export default UserDetails