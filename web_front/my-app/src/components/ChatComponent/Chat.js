import React, { useState } from 'react'
import io from "socket.io-client";
import { useCookies } from 'react-cookie';

const Chat = (props) =>{
  const [userId, setUserId] = useState(props.match.params.id);
  const [receiverId, setreceiverId] = useState();
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const [cookies, setCookie] = useCookies(['login']);
  if(userId===undefined){
    setUserId(cookies.login)
  }
  const socket = io('localhost:8000');
  socket.on('RECEIVE_MESSAGE', function(data){
    addMessage(data);
  });
  const addMessage = data => {
    console.log(data);
    setMessages([...messages, data])
    console.log(messages);
  };
  const sendMessage = ev => {
    ev.preventDefault();
    socket.emit('SEND_MESSAGE', {
        author: userId,
        receiver: receiverId,
        message: message
    })
    setMessage('')
  }
  return (
    <div className="container">
      <div className="row">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <div className="card-title">Global Chat</div>
              <div className="messages">
                {messages.map(message => {
                  return (
                      <div>{message.author} to {message.receiver}: {message.message}</div>
                  )
                })}
              </div>
            </div>
            <div className="card-footer">
                <input type="text" placeholder="ReceiverId" value={receiverId} onChange={ev => setreceiverId(ev.target.value)} className="form-control"/>
                <br/>
                <input type="text" placeholder="Message" className="form-control" value={message} onChange={ev => setMessage(ev.target.value)}/>
                <br/>
                <button onClick={sendMessage} className="btn btn-primary form-control">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Chat
