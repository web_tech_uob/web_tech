import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import { Card, Container, Row, Col, Button, Navbar,  Jumbotron } from 'react-bootstrap';
import './Registration.css'
import { useCookies } from 'react-cookie';
import { LayoutNoSignin } from '../LayoutComponent';
const Reg = (props) => {
  const [cookies, setCookie] = useCookies(['login']);
  useEffect(()=>{
    if(props.login){
      setCookie("login",props.user.id)
    } 
  },[cookies.login,props.login])
  if(cookies.login>0){
    window.location.replace('/')
  }
  
  return(
    <div>
    <LayoutNoSignin verifyMember={props.verifyMember} message = {props.message}/>
      <Container>
          <Row>
            <Col>
              <Container id = "jumbotron">
                <Jumbotron>
                  <h1>Welcome to the Bristol Social Network</h1>
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                  <Link to = "/signup">
                    <Button>Sign up here!</Button>
                  </Link>

                  <Row style = {{align:'center'}}>
                  <h2 className="text-center" style={{marginTop:'2rem', width:'18rem'}}>More Information</h2>
                  </Row>

                  <Row>

                    <Col>
                      <Card style={{width: '18rem', marginTop: '1rem'}}>
                        <Card.Img variant="top" src="/images/team.jpg" />
                          <Card.Body>
                            <Card.Title>The Team</Card.Title>
                              <Card.Text>If you would like to find out more about the Network
                                      and the people who created it please click the button below!</Card.Text>
                              <Link to = "/about">
                                <Button>About us!</Button>
                              </Link>
                          </Card.Body>
                        </Card>
                    </Col>

                  <Col>
                    <Card style={{width: '23.5rem', marginTop: '1rem'}} >
                      <Card.Img variant="top" src="/images/office.jpg"/>
                        <Card.Body>
                          <Card.Title>The Office</Card.Title>
                            <Card.Text>If you would like to see more of the University of Bristol
                            please click the button below!</Card.Text>
                            <Link to = "/about">
                              <Button>See more!</Button>
                            </Link>
                        </Card.Body>
                      </Card>
                  </Col>

                  <Col>
                    <Card style={{width: '18rem', marginTop: '1rem'}} >
                      <Card.Img variant="top" src="/images/technology.png"/>
                        <Card.Body>
                          <Card.Title>Blog</Card.Title>
                            <Card.Text>If you would like to find out more about the things that are here
                            please click the button below!</Card.Text>
                            <Link to = "/about">
                              <Button>Read more!</Button>
                            </Link>
                        </Card.Body>
                      </Card>
                  </Col>

                  </Row>

                </Jumbotron>
              </Container>
            </Col>
          </Row>
      </Container>

      <Navbar>
          <Navbar.Text>This website was created on 19-02-2019 by the University
          of Bristol's Computer Science department</Navbar.Text>
      </Navbar>

    </div>
  );
}

export default Reg
