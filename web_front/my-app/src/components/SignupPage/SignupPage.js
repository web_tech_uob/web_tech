import React, { useState } from 'react'
import { Form, Button, Row, Col } from 'react-bootstrap'
import  { Redirect } from 'react-router-dom'
import { LayoutNoSignin } from '../LayoutComponent'
import './signup.css'
const checkPassword = (passwd) =>
{
  if((passwd.includes("@")||passwd.includes("!")||passwd.includes("#")||passwd.includes("$"))&&(passwd.charAt(0).toUpperCase()===passwd.charAt(0)))
  {return true;}
  return false;
}

const SignupPage = (props) => {
  // Initialise the state
  const [foreName, setForeName] = useState('')
  const [surName, setSurName] = useState('')
  const [userName, setUserName] = useState('')
  const [birth, setBirth] = useState('')
  const [password, setPassword] = useState('')
  // Form is valid
  const [validated, setValidated] = useState(false)
  // Password is valid
  const [passwordisValid,setPasswordIsValid] = useState("");


  // propTypes = {
  //   onSave: PropTypes.func.isRequired,
  // }
  // Handle submit and send the action
  const handleSubmit = (e) => {
    const form = e.currentTarget;
    if(form.checkValidity()){
      e.preventDefault();
      e.stopPropagation();
    }
    setValidated(true);
    const formValues = {
      foreName,
      surName,
      userName,
      birth,
      password
    }
    if(checkPassword(password)){
      setPasswordIsValid("");
      props.createMember(formValues);
    }else{
      setPasswordIsValid("Password Need to have !, @, # and $ and the first character needs to be uppercase.");
    }
  }
  if(props.user.id){
    return <Redirect to='/registration'/>
  }else{
    return(
      <div>
        <LayoutNoSignin verifyMember={props.verifyMember}/>
        <div className="title mt-2">Create Your First Account</div>
          <Form className="formbox mt-4 mb-4" noValidate validated={validated}>
            <Row>
              <Col>
                <Form.Group controlId="userName">
                  <Form.Label>Username</Form.Label>
                  <Form.Control type="email" placeholder="Enter userName" onChange={e => setUserName(e.target.value)} required/>
                  <Form.Text variant="danger">Please Provide Username by E-mail</Form.Text>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group controlId="foreName">
                    <Form.Label>Forename</Form.Label>
                    <Form.Control type="text" placeholder="Enter foreName" onChange={e => setForeName(e.target.value)} required />
                    <Form.Control.Feedback type="invalid">Please Provide Forename</Form.Control.Feedback>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group controlId="lastName">
                    <Form.Label>Surname</Form.Label>
                    <Form.Control type="text" placeholder="Enter lastName" onChange={e => setSurName(e.target.value)} required/>
                    <Form.Control.Feedback type="invalid">Please Provide Surname</Form.Control.Feedback>
                </Form.Group>     
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required/>
                  <Form.Control.Feedback type="invalid">Please Provide Password</Form.Control.Feedback>
                  <Form.Text className="text-danger">{passwordisValid}</Form.Text>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group controlId="birth">
                  <Form.Label>Birth</Form.Label>
                  <Form.Control type="date" placeholder="Enter birth" onChange={e => setBirth(e.target.value)} required/>
                  <Form.Control.Feedback type="invalid">Please Provide Birth</Form.Control.Feedback>
                </Form.Group>  
              </Col>
            </Row>
                    
            <Button variant="primary" type="button" onClick={e=> handleSubmit(e)}>
            Submit
            </Button>
        </Form>
      </div>
    )
  }
}

export default SignupPage
