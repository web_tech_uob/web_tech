import { connect } from 'react-redux'
import { SignupPage } from '../components/SignupPage'
import * as actions from '../actions'

const mapStateToProps = (state) => {
  const user =  state.app.user
  return { user }
}

const mapDispatchToProps = (dispatch) => ({
  createMember: (member) => dispatch(actions.createMember(member)),
  verifyMember: (member) => dispatch(actions.verifyMember(member)),
})

const Signup = connect(mapStateToProps, mapDispatchToProps)(SignupPage)

export default Signup
