import { connect } from 'react-redux'
import { Forum } from '../components/ForumComponent'
import * as actions from '../actions'

const mapStateToProps = (state) => {
  const post =  state.app.post
  const postBoard = state.app.postBoard
  return { post,postBoard }
}

const mapDispatchToProps = (dispatch) => ({
   addMessage: (post) => dispatch(actions.addMessage(post)),
   getMessage: (query) => dispatch(actions.getMessage(query)),
})

const ForumPage = connect(mapStateToProps, mapDispatchToProps)(Forum)

export default ForumPage
