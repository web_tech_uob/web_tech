import { connect } from 'react-redux'
import { Profile } from '../components/UserComponent'
import * as actions from '../actions'

const mapStateToProps = (state) => {
  const profile =  state.app.profile
  const failure = state.app.failure
  return { profile, failure }
}

const mapDispatchToProps = (dispatch) => ({
   updateMember: (member) => dispatch(actions.updateMember(member)),
   fetchMember: (query) => dispatch(actions.fetchMember(query))
})

const UserProfile = connect(mapStateToProps, mapDispatchToProps)(Profile)

export default UserProfile
