import { connect } from 'react-redux'
import { Profile } from '../components/SettingComponent'
import * as actions from '../actions'

const mapStateToProps = (state) => {
  const profile =  state.app.profile
  return { profile }
}

const mapDispatchToProps = (dispatch) => ({
   updateMember: (member) => dispatch(actions.updateMember(member)),
   fetchMember: (query) => dispatch(actions.fetchMember(query))
})

const SettingUserProfile = connect(mapStateToProps, mapDispatchToProps)(Profile)

export default SettingUserProfile
