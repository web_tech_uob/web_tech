import { connect } from 'react-redux'
import { SecondHand } from '../components/SecondHandComponent'
import * as actions from '../actions'

const mapStateToProps = (state) => {
  const product =  state.app.product
  const board = state.app.board
  const categories = state.app.categories
  const display = state.app.display
  const message = state.app.message

  const productCreated = state.app.productCreated
  return { product, board, categories, display, productCreated,message }
}

const mapDispatchToProps = (dispatch) => ({
   createProduct: (product) => dispatch(actions.createProduct(product)),
   fetchProduct: (query) => dispatch(actions.fetchProduct(query)),
   fetchCategory: (query) => dispatch(actions.fetchCategory(query)),

})

const SecondHandPage = connect(mapStateToProps, mapDispatchToProps)(SecondHand)

export default SecondHandPage