import { connect } from 'react-redux'
import { Reg } from '../components/RegistrationComponent'
import * as actions from '../actions'

const mapStateToProps = (state, ) => {
  const user =  state.app.user
  const login = state.app.login
  const message = state.app.message
  return { user, login, message }
}

const mapDispatchToProps = (dispatch) => ({
  verifyMember: (member) => dispatch(actions.verifyMember(member)),
})

const Signin = connect(mapStateToProps, mapDispatchToProps)(Reg)

export default Signin
