import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware, { END } from 'redux-saga'

import rootReducer from '../reducer'
import rootSaga from '../sagas'

export default function configureStore () {
  const sagaMiddleware = createSagaMiddleware()
  const middlewares = [sagaMiddleware]
  const enhancers = []


  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const store = createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(...middlewares),
      ...enhancers
    )
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducer', () =>
      store.replaceReducer(require('../reducer').default)
    )
  }

  sagaMiddleware.run(rootSaga)
  // sagaMiddleware.run(watchCreateMember)
  store.close = () => store.dispatch(END)

  return store
}
