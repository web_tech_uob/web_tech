import React, { Component } from "react";
import { About } from './components/AboutComponent';
import SecondHand from './containers/SecondHand'
import Signup from './containers/Signup'
import Signin from './containers/SignIn'
import Chat from './containers/Chat'
import SettingUserProfile from './containers/SettingUserProfile'
import UserProfile from './containers/UserProfile'
import Forum from './containers/Forum'
import { NoSignInError } from './components/ErrorPage'
import { Errorpage } from './components/ErrorPage'

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component{
  
  render(){
    if(document.cookie.split(';').filter((item) => item.includes('login=')).length){
      console.log('login')
      
      return(
        <Router>
            <div className="h-100">
              <Switch>
              <Route  path="/signup" component={Signup}/>
              <Route  path="/registration" component={Signin}/>
              <Route path="/" exact component={Forum} />
              <Route exact path="/secondhand" component={SecondHand} />
              <Route exact path="/setting/profile" component={SettingUserProfile} />
              <Route exact path="/profile/:id?" component={UserProfile} />
              <Route path="/chat/:id?" component={Chat} />
              <Route component={NoSignInError}/>
              </Switch>         
            </div>
        </Router>
      );
    }else{
      if(window.location.pathname==='/'){
        window.location.replace('/registration')
      }
      return(
        <Router>
          <div>
            <Switch>
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/registration" component={Signin}/>
            <Route exact path="/about" component={About} />
            <Route component={Errorpage}/>
            </Switch>
          </div>
        </Router>
      )
    }
  }
}

export default App;
