export const FETCH_MEMBER = 'app/FETCH_MEMBER'
export const VERIFY_MEMBER = 'app/VERIFY_MEMBER'
export const CREATE_MEMBER = 'app/CREATE_MEMBER'
export const UPDATE_MEMBER = 'app/UPDATE_MEMBER'
export const DELETE_MEMBER = 'app/DELETE_MEMBER'

export const CREATE_PRODUCT = 'app/CREATE_PRODUCT'
export const FETCH_PRODUCT = 'app/FETCH_PRODUCT'
export const FETCH_CATEGORY = 'app/FETCH_CATEGORY'

export const ADD_MESSAGE = 'app/ADD_MESSAGE'
export const GET_MESSAGE = 'app/GET_MESSAGE'
