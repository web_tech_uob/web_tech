import { take, fork, call, all } from 'redux-saga/effects'
import * as api from './api'
import * as types from '../constants'

export function * watchCreateMember() {
  while (true) {
    const { payload: member } = yield take(types.CREATE_MEMBER)
    yield call(api.createMember, member)
  }
}
export function * watchUpdateMember() {
  while (true) {
    const { payload: member } = yield take(types.UPDATE_MEMBER)
    yield call(api.updateMember, member)
  }
}
export function * watchVerifyMember() {
  while (true) {
    const { payload: member } = yield take(types.VERIFY_MEMBER)
    yield call(api.verifyMember, member)
  }
}
export function * watchFetchMember() {
  while (true) {
    const { payload: query } = yield take(types.FETCH_MEMBER)
    yield call(api.fetchMember, query)
  }
}

export function * watchCreateProduct() {
  while (true) {
    const { payload: product } = yield take(types.CREATE_PRODUCT)
    yield call(api.createProduct, product)
  }
}
export function * watchFetchProduct() {
  while (true) {
    const { payload: query } = yield take(types.FETCH_PRODUCT)
    yield call(api.fetchProduct, query)
  }
}
export function * watchFetchCategory() {
  while (true) {
    const { payload: query } = yield take(types.FETCH_CATEGORY)
    yield call(api.fetchCategory, query)
  }
}
export function * watchAddMessage() {
  while (true) {
    const { payload: post } = yield take(types.ADD_MESSAGE)
    yield call(api.addMessage, post)
  }
}
export function * watchGetMessage() {
  while (true) {
    const { payload: query } = yield take(types.GET_MESSAGE)
    yield call(api.getMessage, query)
  }
}
export default function * () {
  yield all([
    fork(watchCreateMember),
    fork(watchUpdateMember),
    fork(watchVerifyMember),
    fork(watchFetchMember),
    fork(watchCreateProduct),
    fork(watchFetchProduct),
    fork(watchFetchCategory),
    fork(watchAddMessage),
    fork(watchGetMessage),
  ])
}
