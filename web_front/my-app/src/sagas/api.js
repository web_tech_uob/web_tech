import * as actions from '../actions'
import { put, call } from 'redux-saga/effects'
import * as api from '../services'

export function * callApi (apiAction, apiFn, options) {
  const { request, success, failure } = apiAction
  const { meta } = options
  try {
    yield put(request(meta))
    const response = yield call(apiFn, options)
    console.log(response)
    yield put(success(meta, response))
  } catch (error) {
    console.log(error)
    yield put(failure(meta, error))
  }
}

export const createMember = callApi.bind(null, actions.members.create, api.Member.create)
export const updateMember = callApi.bind(null, actions.members.update, api.Member.update)
export const verifyMember = callApi.bind(null, actions.members.verify, api.Member.verify)
export const fetchMember = callApi.bind(null, actions.members.fetch, api.Member.fetch)

export const createProduct = callApi.bind(null, actions.products.create, api.Product.create)
export const fetchProduct = callApi.bind(null, actions.products.fetch, api.Product.fetch)
export const fetchCategory = callApi.bind(null, actions.products.category, api.Product.category)

export const addMessage = callApi.bind(null, actions.post.add, api.Post.add)
export const getMessage = callApi.bind(null, actions.post.get, api.Post.get)

// function * watchCallApi () {
//   while (true) {
//     const action = yield take(action => ~action.type.indexOf('api/'))
//     // if (action.error && action.payload.response && action.payload.response.status === 401) {
//     //   const lastURL = yield select(selectors.getPathname)
//     //   const lastURLOptions = { path: '/' }
//     //   return yield [call(cookie.save, 'last_url', lastURL, lastURLOptions), put(login())]
//     // }

//     // if (action.error && action.payload.message === 'Unauthorized') yield put(login())
//   }
// }

// export default function * () {
//   yield [
//     fork(watchCallApi)
//   ]
// }
