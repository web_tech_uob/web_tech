import { fork , all } from 'redux-saga/effects'
import app from './app'
// import api from './api'


export default function * rootSaga () {
  yield all([
    fork(app),
    // fork(api),
  ])
}
