import { handleActions } from 'redux-actions'
import * as types from '../constants'
import { stat } from 'fs';

export const initialState = {
  login: false,
  user: [],
  profile: [],
  product:[],
  board:[],
  categories:[],
  display:false,
  failure:false,
  message:"",
  productCreated:false,
  post:[],
  postBoard:[]
}

const app = handleActions({
  [types.CREATE_MEMBER_SUCCESS]: (state, action) => ({ ...state, user: action.payload }),
  [types.UPDATE_MEMBER_SUCCESS]: (state, action) => ({ ...state, user: action.payload, profile:action.payload}),
  [types.FETCH_MEMBER_SUCCESS]: (state, action) => ({ ...state, profile: action.payload }),
  [types.FETCH_MEMBER_FAILURE]: (state, action) => ({ ...state, failure: true }),
  [types.VERIFY_MEMBER_SUCCESS]: (state, action) => ({ ...state, user: action.payload.user, login:action.payload.login }),
  [types.VERIFY_MEMBER_FAILURE]: (state, action)=> ({ ...state, message: "User name or password is incorrect"
  }),
  [types.CREATE_PRODUCT_SUCCESS]: (state, action) => ({ ...state, product: action.payload, board: {
    productNames: [...state.board.productNames, action.payload.productName],
    descriptions: [...state.board.descriptions, action.payload.description],
    photos: [...state.board.photos, action.payload.photo],
    photopaths: [...state.board.photopaths, action.payload.photopath],
    userNames:[...state.board.userNames, action.payload.userName],
    userIds:[...state.board.userIds, action.payload.userId],
    uploadTime:[...state.board.uploadTime, action.payload.uploadTime]
  }, productCreated:true}),
  [types.FETCH_PRODUCT_SUCCESS]: (state, action) => ({ ...state, board: action.payload }),
  [types.FETCH_CATEGORY_SUCCESS]: (state, action) => ({ ...state, categories: action.payload, display:true }),
  [types.ADD_MESSAGE_SUCCESS]: (state, action) => ({ ...state, post: action.payload, postBoard: {
      titles: [...state.postBoard.titles, action.payload.title],
      messages: [...state.postBoard.messages, action.payload.message],
      photos: [...state.postBoard.photos, action.payload.photo],
      photopaths: [...state.postBoard.photopaths, action.payload.photopath],
      userNames:[...state.postBoard.userNames, action.payload.userName],
      postTimes:[...state.postBoard.postTimes, action.payload.postTime],
      userIds:[...state.postBoard.userIds, action.payload.userId],

  } }),
  [types.GET_MESSAGE_SUCCESS]: (state, action) => ({ ...state, postBoard: action.payload }),


}, initialState)
export default app
