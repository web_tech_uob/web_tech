# UoB Web Technology


## Contributor:

-	Yu-Ting Lan, cq18784, Computer Science Conversion
-	Otto Brookes, dl18206,  Computer Science Conversion
-	Ting-Hung Cheng, hu18727, Advanced Computing

## Plan:
- 	Topic
	- 	Social connection between UoB computer science students and extend to all UoB students 	
- Purpose
	- To build up the socail network and provide organised information for each Student
- Stage
	- Basic
	1. Build up login, logut, profile page, and about us page 
	2. Build up Useful Board, such as information board and second-hand transaction board
- Extension ( If time avavailbe )
	1. Build up the message board and can functionally categorise into different tag
	2. Build up the course calendar
## Tool 

#### - MariaDB
#### - HTML, CSS, JS
#### - React
#### - Saga
#### - Redux
#### - Express
####  - Socket

## Usage:
1. Go to `web` folder and install the dependencies by using `npm install` or install packages separately, if `npm install` doesn't work. 
2. Go to `web_front/my-app` folder and install the dependencies by using `npm install` or install packages separately, if `npm install` doesn't work. 
3. Go to `web` folder and run the server by using `npm start`.
4. Go to `web_front/my-app` folder and run the server  by using `npm start`.