var { buildSchema } = require('graphql');

exports.schema = buildSchema(`
  type Query {
    member: [Member]
  }

  type Member {
    id: ID!
    foreName: String
    surName: String
  }
`);

exports.rootValue = {
  hello: () => 'Hello world!',
};

