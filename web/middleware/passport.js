// const JWTStrategy = require('passport-jwt').Strategy;
// const ExtractJWT = require('passport-jwt').ExtractJwt;
var LocalStrategy = require( 'passport-local' ).Strategy;
const models = require("../models")
const User = models.user;

// const opts = {};

// opts.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
// opts.secretOrKey = 'secret';

// export.localStrategy2 = new JWTStrategy(opts, (jwt_payload, done) => {
//   User.findOne({
//     where:{
//       userName: username,
//     }
//   }).then(user=>{
//     console.log(User.encryptPassword(password, user.salt()))
//     console.log(user.password())
//     if (!user) {
//       return done(null, false, { message: 'Incorrect username.' })
//     }
//     if (User.encryptPassword(password, user.salt()) != user.password()) {
//       return done(null, false, { message: 'Incorrect password.' })
//     }else{
//       console.log('test')
//       return done(null, user);
//     } 
//   })
// }
// )

exports.localStrategy = new LocalStrategy({
  usernameField: 'userName',
  passwordField: 'password'
},
  function (username, password, done) {
    User.findOne({
      where:{
        userName: username,
      }
    }).then(user=>{
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' })
      }
      if (User.encryptPassword(password, user.salt()) != user.password()) {
        return done(null, false, { message: 'Incorrect password.' })
      }else{
        return done(null, user);
      }
      
    })
  }
)