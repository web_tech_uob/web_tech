const crypto = require('crypto')
const jwt = require('jsonwebtoken');

module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define('user', {
	  foreName: {
		  type: Sequelize.STRING
    },
    surName: {
      type: Sequelize.STRING
    },
    userName: {
      type: Sequelize.STRING,
      unique:true,
      validate:{
        isEmail:true,
      }
    },
    password: {
      type: Sequelize.STRING,
      get(){
        return ()=>this.getDataValue('password')
      }
    },
    salt: {
      type:Sequelize.STRING,
      get(){
        return ()=>this.getDataValue('salt')
      }
    },
    biography: {
      type: Sequelize.STRING
    },
    birth: {
      type: Sequelize.DATE
    },
    status:{
      type: Sequelize.INTEGER
    },
    photo: {
      type: Sequelize.TEXT
    },
    major:{
      type: Sequelize.STRING
    },
    address:{
      type: Sequelize.STRING
    }
	});
	User.generateSalt = function() {
    return crypto.randomBytes(16).toString('base64')
  }

  User.encryptPassword = function(plainText, salt) {
    return crypto
        .createHash('RSA-SHA256')
        .update(plainText)
        .update(salt)
        .digest('hex')
  }
  
  User.generateJWT = (email, _id) => {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
      email: email,
      id: _id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
  }

  const toAuthJSON = user => {
    return {
      _id: user._id,
      email: user.email,
      token: User.generateJWT(user.email, user._id),
    };
  };

  const setSaltAndPassword = user => {
    if (user.changed('password')) {
        user.salt = User.generateSalt()
        user.password = User.encryptPassword(user.password(), user.salt())
    }
  }
  User.beforeCreate(setSaltAndPassword)
  User.beforeUpdate(setSaltAndPassword)

	return User;
}