'use strict';

module.exports = (sequelize, Sequelize) => {
	const Post = sequelize.define('post', {
    title: {
		  type: Sequelize.STRING
    },
    message: {
      type: Sequelize.TEXT
    },
    userId:{
      type: Sequelize.INTEGER
    },
    photo: {
      type: Sequelize.TEXT
    },
    photopath:{
      type: Sequelize.STRING
    }
    }
    );
    Post.associate = function(models){
    Post.belongsTo(models.user,{
      foreignKey: {
        allowNull: false
      }
    });
  };
  return Post;
}
