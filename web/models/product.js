'use strict';

module.exports = (sequelize, Sequelize) => {
	const Product = sequelize.define('product', {
    productName: {
		  type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    photo: {
      type: Sequelize.TEXT
    },
    photopath:{
      type: Sequelize.STRING
    },
    category:{
      type: Sequelize.INTEGER
    },
    userId:{
      type: Sequelize.INTEGER
    }
    }
    );
    Product.associate = function(models){
    Product.belongsTo(models.user,{
      foreignKey: {
        allowNull: false
      }
    });
  };
  return Product;
}
