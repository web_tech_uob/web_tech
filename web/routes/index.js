var express = require('express');
var passport = require('passport');
var router = express.Router();
// const { localStrategy } = require("../middleware/passport")

const User = require('../controller/user.js')
const Product = require('../controller/product.js')
const Post = require('../controller/post.js')

// passport.use( 'local', localStrategy );

// router.post('/users/username',User.findUser);
router.get('/users', User.findAll);
router.get('/users/:id', User.findById);
router.post('/users/create', User.create);
router.put('/users/:id', User.update);
router.delete('/users/:id', User.delete);
router.post('/users/username',
  passport.authenticate('local'),
  (req, res) => {
    console.log(req.session)
    var userInfo = {
      user: req.user,
      login:true
    };
    res.send(userInfo);
  }

);

router.post('/products/create', Product.create);
router.get('/products', Product.findAll);
router.post('/categories',Product.findByCategory);

router.post('/forum/add', Post.create);
router.get('/forum', Post.findAll);


module.exports = router;
