var createError = require('http-errors');
var express = require('express');
var graphqlHTTP = require('express-graphql');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var cors = require('cors');
const session = require('express-session');
var passport = require( 'passport' );
var flash = require('connect-flash');

var app = express();

const { schema, rootValue } = require("./middleware/graphql")
const { localStrategy } = require("./middleware/passport")






// view engine setup
app.engine('pug', require('pug').__express)

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cors());
app.use(logger('dev'));
app.use(express.json({limit: '10mb', extended: true}));
app.use(express.urlencoded({limit: '10mb', extended: false }));
app.use(cookieParser());
app.use('/static',express.static(path.join(__dirname, 'public')));

app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ 
  secret: 'passport-tutorial', 
  cookie: { maxAge: 60000 }, 
  resave: true, 
  saveUninitialized: true 
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

passport.serializeUser(function (user, done) {
  console.log('serial')
  done(null, user.id);
  console.log(user.id)
});

passport.deserializeUser(function (id, done) {
  console.log("deserial")
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

passport.use( 'local', localStrategy );


app.use('/', indexRouter);

app.use('/graphql', graphqlHTTP({
  schema,
  rootValue,
  graphiql: true,
}));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// app.use( BodyParser.urlencoded( { extended: false } ) );
// app.use( BodyParser.json() );




module.exports = app;
