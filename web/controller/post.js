
const models = require("../models")
const User = models.user;
const Post = models.post;
const fs = require('fs');
module.exports.test = (req,res) => {
	res.send("success")
}
// // Post a User
module.exports.create = (req, res) => {
	const path = 'public/images/post/'+req.body.userId;
	fs.promises.mkdir(path,{recursive:true}).catch(console.error).then(x=>{
		const img = req.body.photo;
		const data = img.replace(/^data:image\/\w+;base64,/, "");
		const filepath = path+'/'+req.body.fileName
		const buf = new Buffer(data, 'base64');
		fs.writeFile(filepath, buf,function (err) {
			if (err) throw err;
			// console.log('Saved!');
		 });
	})
	// Save to MariaDB database
	return Post.create({
      	title: req.body.title,
		message: req.body.message,
		userId: req.body.userId,
		photo: req.body.fileName,
		photopath: 'static/images/post/'+req.body.userId+'/'+req.body.fileName,
		})
		.then(post => Post.findAll(
			{
				where:{id:post.id},
				attributes: { exclude: ["updatedAt"] },
				include:[{model:User,}]
			}
		)).then(e=>{
			var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
			"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
			var datalist={
				title: e[0].dataValues.title,
				photo: e[0].dataValues.id,
				photopath:e[0].dataValues.photopath,
				message: e[0].dataValues.message,
				userName : e[0].dataValues.user.dataValues.foreName+','+e[0].dataValues.user.dataValues.surName,
				userId : e[0].dataValues.user.dataValues.id,
				postTime : e[0].dataValues.createdAt.getDay()+' '+(months[e[0].dataValues.createdAt.getMonth()])+' '+e[0].dataValues.createdAt.getFullYear()
				+' '+e[0].dataValues.createdAt.getHours()+':'+e[0].dataValues.createdAt.getMinutes()
			}
			res.json(datalist);
			// console.log(file.name)
		})
		.catch(error => {
			console.log(error)
			res.status(400).send(error)})
};

module.exports.findAll = (req, res) => {
	return Post.findAll({
			attributes: { exclude: ["updatedAt"] },
			include:[{model:User,}],
			// order:[['createdAt','DESC']]
		})
		.then(post => {
			var data = {
				titles: [],
				messages: [],
				photos: [],
				photopaths: [],
				userNames : [],
				userIds : [],
				postTimes : []
			}
			var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
			"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
			for (e in post){

				// console.log(post[e]);
				data.titles.push(post[e].dataValues.title);
				data.messages.push(post[e].dataValues.message);
				data.userNames.push(post[e].dataValues.user.dataValues.foreName+','+post[e].dataValues.user.dataValues.surName);
				data.photopaths.push(post[e].dataValues.photopath);
				data.photos.push(post[e].dataValues.id);
				data.userIds.push(post[e].dataValues.user.dataValues.id);
				data.postTimes.push(post[e].dataValues.createdAt.getDay()+' '+(months[post[e].dataValues.createdAt.getMonth()])+' '+post[e].dataValues.createdAt.getFullYear()
				+' '+post[e].dataValues.createdAt.getHours()+':'+post[e].dataValues.createdAt.getMinutes());
			}
			console.log(data)
			res.json(data);
		})
		.catch(error => {
			console.log(error);
			res.status(400).send(error)
		})
};
