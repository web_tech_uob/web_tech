const models = require("../models")
const User = models.user;
const fs = require('fs');

module.exports.test = (req,res) => {
	res.send("success")
}
// // Post a User
module.exports.create = (req, res) => {	
	// Save to MariaDB database
	return User.create({  
      foreName: req.body.foreName,
      surName: req.body.surName,
      userName: req.body.userName,
      password: req.body.password,
      biography: req.body.biography,
      birth: req.body.birth,
      status: req.body.status
		})
		.then(user => {		
			// Send created customer to client
			res.json(user);
		})
		.catch(error => res.status(400).send(error))
};

module.exports.findUser = (req,res) =>{
	return User.findOne({
		where:{
			userName: req.body.userName,
			// password: req.body.password
		}
	}).then(user=>{
		res.json({
			user: user,
			login:User.encryptPassword(req.body.password, user.salt()) === user.password()
		})
	})
	.catch(error=>res.status(400).send(error))

}
// Fetch all Users
module.exports.findAll = (req, res) => {
	return User.findAll({
			attributes: { exclude: ["createdAt", "updatedAt"] }
		})
		.then(users => {
			res.json(users);
		})
		.catch(error => res.status(400).send(error))
};
 
// Find a User by Id
module.exports.findById = (req, res) => {	
	return User.findById(req.params.id,
				{attributes: { exclude: ["createdAt", "updatedAt"] }}
			)
			.then(user => {
					if (!user){
						return res.status(404).json({message: "User Not Found"})
					}
					var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
					"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
					var data = {
						foreName: user.dataValues.foreName,
						surName: user.dataValues.surName,
						userName: user.dataValues.userName,
						biography: user.dataValues.biography,
						birth: user.dataValues.birth.getDay()+' '+(months[user.dataValues.birth.getMonth()])+' '+user.dataValues.birth.getFullYear(),
						status:user.dataValues.status,
						photo:user.dataValues.photo,
						major:user.dataValues.major,
						address:user.dataValues.address

					}
					console.log(data)
					res.status(200).json(data)
				}
			)
			.catch(error => {
				console.log(error)

				res.status(400).send(error)
			});
};
 
// Update a User
module.exports.update = (req, res) => {
	if(req.body.fileName!==undefined){
		const path = 'public/images/'+req.params.id+'/selfie';
		date = new Date()
		fs.promises.mkdir(path,{recursive:true}).catch(console.error).then(x=>{
			const img = req.body.photo;
			const data = img.replace(/^data:image\/\w+;base64,/, "");
			const filepath = path+'/'+date.getDay()+date.getMonth()+date.getTime()+req.body.fileName
			const buf = new Buffer(data, 'base64');
			fs.writeFile(filepath, buf,function (err) {
				if (err) throw err;
				console.log('Saved!');
			});
	})
	}
	return User.findByPk(req.params.id)
		.then(
			user => {
				if(!user){
					res.status(404).json({
						message: 'User Not Found',
					});
				}
				else{ 
					currentStatus= user.dataValues.status;
					currentForename= user.dataValues.foreName;
					currentSurName= user.dataValues.surName;
					currentBirth= user.dataValues.birth;
					currentPhoto= user.dataValues.photo;
					currentMajor= user.dataValues.major;
					currentBio= user.dataValues.biography;
					currentAdds= user.dataValues.address;

					if(req.body.status!==""){currentStatus = req.body.status}
					if(req.body.foreName!==undefined){currentForename = req.body.foreName}
					if(req.body.surName!==undefined){currentSurName = req.body.surName}
					if(req.body.birth!==undefined){currentBirth = req.body.birth}

					if(req.body.fileName!==undefined){currentPhoto = 'static/images/'+req.params.id+'/selfie/'+date.getDay()+date.getMonth()+date.getTime()+req.body.fileName}
					if(req.body.major!==undefined){currentMajor = req.body.major}
					if(req.body.address!==undefined){currentAdds = req.body.address}
					if(req.body.biography!==undefined){currentBio = req.body.biography}

					user.update({
                    foreName: currentForename,
                    surName: currentSurName,
						  birth: currentBirth,
						  photo: currentPhoto,
						  biography:currentBio,
						  major:currentMajor,
						  address:currentAdds,
                    status: currentStatus 
					})				
					.then(() => res.status(200).json(user))
					.catch((error) => {
						console.log(error)
						res.status(400).send(error)});
				}
			}
			)
		.catch((error) => res.status(400).send(error));			 
};
 
// Delete a User by Id
module.exports.delete = (req, res) => {
	return User
			.findById(req.params.id)
			.then(user => {
				if(!user) {
					return res.status(400).send({
						message: 'User Not Found',
					});
				}

				return user.destroy()
												.then(() => res.status(200).json({message: "Destroy successfully!"}))
												.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
};