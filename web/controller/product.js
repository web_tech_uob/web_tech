
const models = require("../models")
const User = models.user;
const Product = models.product;
const fs = require('fs');
module.exports.test = (req,res) => {
	res.send("success")
}
// // Post a User
module.exports.create = (req, res) => {	
	const path = 'public/images/'+req.body.userId;
	fs.promises.mkdir(path,{recursive:true}).catch(console.error).then(x=>{
		const img = req.body.photo;
		const data = img.replace(/^data:image\/\w+;base64,/, "");
		const filepath = path+'/'+req.body.fileName
		const buf = new Buffer(data, 'base64');
		fs.writeFile(filepath, buf,function (err) {
			if (err) throw err;
			// console.log('Saved!');
		 });
	})
	// Save to MariaDB database
	return Product.create({  
      productName: req.body.productName,
		description: req.body.description,
		photo: req.body.fileName,
		photopath: 'static/images/'+req.body.userId+'/'+req.body.fileName,
		category:req.body.category,
		userId: req.body.userId,
		})
		.then(product => Product.findAll(
			{
				where:{id:product.id},
				attributes: { exclude: ["updatedAt"] },
				include:[{model:User,}]
			}
		)).then(e=>{
			var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
			"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
			var datalist={
				productName: e[0].dataValues.productName,
				description: e[0].dataValues.description,
				photo: e[0].dataValues.id,
				photopath:e[0].dataValues.photopath,
				userName : e[0].dataValues.user.dataValues.foreName+','+e[0].dataValues.user.dataValues.surName,
				userId : e[0].dataValues.user.dataValues.id,
				uploadTime: e[0].dataValues.createdAt.getDay()+' '+(months[e[0].dataValues.createdAt.getMonth()])+' '+e[0].dataValues.createdAt.getFullYear()
				+' '+e[0].dataValues.createdAt.getHours()+':'+e[0].dataValues.createdAt.getMinutes()
			}
			res.json(datalist);
			// console.log(file.name)
		})
		.catch(error => {
			// console.log(error)		
			res.status(400).send(error)})
};

module.exports.findAll = (req, res) => {
	return Product.findAll({
			attributes: { exclude: ["updatedAt"] },
			include:[{model:User,}]
		})
		.then(products => {
			var data = {
				productNames: [],
				descriptions: [],
				photos: [],
				photopaths: [],
				categories:[],
				userNames : [],
				userIds : [],
				uploadTime: [],
			}
			var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
			"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
			for (e in products){
			
				data.uploadTime.push(products[e].dataValues.createdAt.getDay()+' '+(months[products[e].dataValues.createdAt.getMonth()])+' '+products[e].dataValues.createdAt.getFullYear()
				+' '+products[e].dataValues.createdAt.getHours()+':'+products[e].dataValues.createdAt.getMinutes());
				data.productNames.push(products[e].dataValues.productName);
				data.descriptions.push(products[e].dataValues.description);
				data.photos.push(products[e].dataValues.id);
				data.photopaths.push(products[e].dataValues.photopath);
				data.categories.push(products[e].dataValues.category);
				data.userNames.push(products[e].dataValues.user.dataValues.foreName+','+products[e].dataValues.user.dataValues.surName);
				data.userIds.push(products[e].dataValues.user.dataValues.id);
				
			}
			// console.log(data)
			res.json(data);
		})
		.catch(error => res.status(400).send(error))
};
module.exports.findByCategory = (req, res) => {
	return Product.findAll({
			where:{
				category: Number(req.body.id), 
			},
			attributes: { exclude: ["updatedAt"] },
			include:[{model:User,}]
		})
		.then(products => {
			var productList = {
				productNames: [],
				descriptions: [],
				photos: [],
				photopaths: [],
				userNames : [],
				userIds : [],
				uploadTime: [],

			}
			var months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", 
			"Jun.", "Jul.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."];
			for (e in products){
				productList.uploadTime.push(products[e].dataValues.createdAt.getDay()+' '+(months[products[e].dataValues.createdAt.getMonth()])+' '+products[e].dataValues.createdAt.getFullYear()
				+' '+products[e].dataValues.createdAt.getHours()+':'+products[e].dataValues.createdAt.getMinutes());
				productList.productNames.push(products[e].dataValues.productName);
				productList.descriptions.push(products[e].dataValues.description);
				productList.photos.push(products[e].dataValues.id);
				productList.photopaths.push(products[e].dataValues.photopath);
				productList.userNames.push(products[e].dataValues.user.dataValues.foreName+','+products[e].dataValues.user.dataValues.surName)
				productList.userIds.push(products[e].dataValues.user.dataValues.id)
			}
			res.json(productList);

		})
		.catch(error => {
			// console.log(error)
			res.status(400).send(error)
		})
};
