### variable declare difference  -- author by david

1. let: it's a blocked scope.
2. const: it's a blocked scope and it can't be redeclaired
3. var: var can be global / function scope it's based on where you declare it and it's able to redeclared

reference: https://dev.to/sarah_chima/var-let-and-const--whats-the-difference-69e


### difference between async await and promise -- author by david
First we will discuss difference between async and sync

The earliest and most straightforward solution to being stuck in the synchronous world was asynchronous callbacks (think setTimeout()).

asynchronous(async) means that you don't need to wait for database or processing time and it will deal with next line command first until get the response.

synchronous(sync) means that program will execute line by line

promise: it's the basic technique under async. we can use "then()" to recall the result when we want to use it.

Second we will look though to async/await

Benefits:
1. Continue using promises
2. Write asynchronous code that looks and feels synchronous
3. Cleans up your syntax and makes your code more human-readable

await: is designed for waiting response from different async function therefore we can use async and await to let code run as synchronous


### what is middleware


### what is function*
The function* declaration (function keyword followed by an asterisk) defines a generator function, which returns a Generator object.

### redux-saga
take这个方法，是用来监听action，返回的是监听到的action对象
call和apply方法与js中的call和apply相似
redux-saga执行副作用方法转化action时，put这个Effect方法跟redux原始的dispatch相似，都是可以发出action
中间件中获取state，那么需要使用select。select方法对应的是redux中的getState
fork方法在第三章的实例中会详细的介绍，这里先提一笔，fork方法相当于web work，fork方法不会阻塞主线程，在非阻塞调用中十分有用。
ref: https://github.com/forthealllight/blog/issues/14


### difference between frontend auth and backend server
The front-end server handles authentication in two ways: either the front-end server authenticates the user itself (either using Basic or forms-based authentication), or it forwards the request anonymously to the back-end server. Either way, the back-end server also performs authentication.



